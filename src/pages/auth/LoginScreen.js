import React from "react";
import {useHistory} from 'react-router-dom';
import { Formik, Field } from "formik";
import * as yup from "yup";
import {
  Box,
  Button,
  ButtonGroup,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { FieldInput } from "components/UI/FieldInput";
import { AuthLayout } from "layout/auth";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "35%",
    padding: "2rem",
    backgroundColor: "#ffffff",
    borderRadius: "10px",
  },
  rootForm: {
    display: "flex",
    flexDirection: "column",
  },
}));
const schemaValidations = yup.object().shape({
  email: yup
    .string()
    .email("El email no es requerido")
    .required("campo requerido"),
  password: yup.string().required("el campo es requerido"),
});

const LoginScreen = () => {
  const classes = useStyles();
  const history = useHistory();
  return (
    <AuthLayout>
      <Box component="div" className={classes.root}>
        <Typography
          style={{ fontWeight: "bold" }}
          variant="h4"
          color="textSecondary"
          gutterBottom
          align="center"
        >
          LAE
        </Typography>
        <div>
          <Formik
            validationSchema={schemaValidations}
            initialValues={{
              email: "",
              password: "",
            }}
          >
            {({ handleSubmit }) => (
              <form
                noValidate
                onSubmit={handleSubmit}
                className={classes.rootForm}
              >
                <Field
                  size="small"
                  name="email"
                  type="email"
                  variant="outlined"
                  placeholder="Correo electronico"
                  style={{ marginBottom: "1rem" }}
                  component={FieldInput}
                />
                <Field
                  size="small"
                  name="password"
                  type="password"
                  variant="outlined"
                  placeholder="Contraseña"
                  style={{ marginBottom: "1rem" }}
                  component={FieldInput}
                />
                <ButtonGroup fullWidth>
                  <Button
                    color="primary"
                    size="medium"
                    variant="contained"
                    fullWidth
                    type="submit"
                  >
                    Iniciar sesión
                  </Button>
                  <Button
                    color="secodary"
                    size="medium"
                    variant="outlined"
                    type="button"
                    fullWidth
                    onClick={() => history.push('/auth/register')}
                  >
                    Registrarme
                  </Button>
                </ButtonGroup>
              </form>
            )}
          </Formik>
        </div>
      </Box>
    </AuthLayout>
  );
};

export { LoginScreen };
