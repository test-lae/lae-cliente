import { HomeScreen } from "pages/HomeScreen";
import React from "react";
import {
  BrowserRouter as Router,
} from "react-router-dom";
import { authRoute } from "./AuthRoute";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";

function AppRouter() {
  const user = {
    logged: false,
  };
  return (
    <Router>
      <PublicRoute
        path="/auth"
        isLoggedIn={user.logged}
        component={authRoute}
      />
      <PrivateRoute
        exact
        strict
        path="/"
        component={HomeScreen}
        isLoggedIn={user.logged}
      />
    </Router>
  );
}

export default AppRouter;
