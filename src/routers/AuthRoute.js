import {Switch, Route, Redirect} from 'react-router-dom';
import React from 'react'
import { LoginScreen } from 'pages/auth/LoginScreen';
import { RegistryScreen } from 'pages/auth/RegistryScreen';

export const authRoute = () => {
  return (
    <Switch>
      <Route exact strict path="/auth/login" component={LoginScreen}/>
      <Route exact strict path="/auth/register" component={RegistryScreen}/>
      <Redirect to="/auth/login"/>
    </Switch>
  )
}