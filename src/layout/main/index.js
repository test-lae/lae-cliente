import React from 'react';
import PropTypes from 'prop-types';
import { CssBaseline, MuiThemeProvider } from '@material-ui/core';
import { Global, css } from '@emotion/react';
import theme from 'config/theme';


const Layout = ({ children }) => {

    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline />
            <Global
                styles={css`
                    :root {
                        --primary: ${theme.palette.primary.main};
                        --primary-dark: ${theme.palette.primary.dark};
                        --primary-light: ${theme.palette.primary.light};
                        --contrastText: ${theme.palette.primary.contrastText};
                        --secondary: ${theme.palette.secondary.main};
                        --error: ${theme.palette.error.main};
                        --warning: ${theme.palette.warning.main};
                        --success: ${theme.palette.success.main};
                    }
                    html {
                        font-size: 100%;
                        box-sizing: border-box;
                    }
                    *,
                    *:before,
                    *:after {
                        box-sizing: inherit;
                    }

                    body {
                        font-family: 'Roboto', 'Open Sans', sans-serif;
                    }
                    img {
                        max-width: 100%;
                    }
                `}
            />
            <main>
                {children}
            </main>
        </MuiThemeProvider>
    )
}

export default Layout;
Layout.propTypes = {
    children: PropTypes.node
}
