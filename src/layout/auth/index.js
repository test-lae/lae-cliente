import React from "react";
import PropTypes from "prop-types";
import imgFondo from "assets/imgs/imgFondo.jpg";
import styled from "@emotion/styled";

const ContainerLayout = styled.div`
  background-image: url(${({ srcUrl }) => srcUrl});
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  background-repeat: no-repeat;
  height: 100vh;
  width: 100%;
`;
export const AuthLayout = ({ children }) => {
  return <ContainerLayout srcUrl={imgFondo}>{children}</ContainerLayout>;
};
AuthLayout.propTypes = {
  children: PropTypes.node.isRequired,
};
