import React from 'react';
import { TextField } from '@material-ui/core';

export const FieldInput = ({
    label,
    placeholder,
    type,
    helperText,
    form: { touched, errors },
    field,
    ...props
}) => {
    return (
        <TextField
            error={touched[field.name] && Boolean(errors[field.name])}
            type={type}
            label={label}
            helperText={touched[field.name] && errors[field.name] ? errors[field.name] : helperText}
            placeholder={placeholder}
            {...field}
            {...props}
        />
    )
}
