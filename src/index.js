import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from 'routers/AppRouter';
import Layout from 'layout/main';

console.log("root: rutas")
const MainApp = () => (
  <Layout>
    <AppRouter />
  </Layout>
)

ReactDOM.render(
  <MainApp />
  ,
  document.getElementById('root')
);
